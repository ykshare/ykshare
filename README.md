# YkShare

## Use cases

- Upload/Download file to SkyNet portals
- Encrypt and decrypt files locally

## Demo

1. [Install dependencies](#install-dependencies)
2. `$ python3 ykshare.py "sia://AADgApbh0RjtrtDT4fdV3aF7D235UcoTByubdm6bkko9ZA#BkvZHWTy_QCkntt41NPaiQ"`
3. Open `YkShare_demo.gif`

<sub>[Or click here!](https://siasky.net/AAAY9zKK9_uExPcGwOypQLVCbFqrPJgZyVXjA65NWGDS7Q)</sub>

## How to use it?

### Install dependencies

```
$ pip install -r requirements.txt --user
```

### Usage


#### Upload

##### Simple

```
$ python3 ykshare.py "<file_to_upload>"

```

##### With full args

```
$ python3 ykshare.py "<file_to_upload>" --skynetportal '{"portal_url": "https://siasky.net", "portal_upload_path": "skynet/skyfile", "portal_file_fieldname": "file", "custom_filename":"}'
```

#### Download

##### Simple

```
$ python3 ykshare.py "sia://xxxxxxxxxxxxx#xxxxxxxxxxx"

```

```
$ python3 ykshare.py "https://URL_OF_A_SKYNET_PORTAL/xxxxxxxxxxxxx#xxxxxxxxxxx"

```

##### With full args

```
$ python3 ykshare.py "sia://xxxxxxxxxxxxx#xxxxxxxxxxx" --skynetportal '{"portal_url": "https://siasky.net"}'

```

## Security

We're finding inspiration on the [Firefox Send project](https://github.com/mozilla/send) to do YkShare.  
We’re using the 128-bit AES-GCM algorithm to encrypt files before uploading them to the server.  
We've based our security implementation on the [ffsend project](https://github.com/nneonneo/ffsend/blob/master/ffsend.py).

### Uploading

1. A new secret key is generated with crypto.getRandomValues
2. The secret key is used to derive more keys via HKDF SHA-256
   * a series of encryption keys for the file, via ECE (AES-GCM)
   * an encryption key for the file metadata (AES-GCM)
3. The file and its metadata are bundle on a bson file.
4. This bson file is encrypted with their corresponding keys
4. The encrypted data are uploaded to the server
5. The share url is returned by the server
6. The secret key is appended to the share url as a #fragment and presented to the user

### Downloading

1. We download the encrypted file using the share url page
2. We decrypt the file by importing the secret key from the url fragment
3. We rebuild the original file by extracting the bson file.

## FAQ

### How long are files available for?

Files are available to be downloaded  for a undefinied time.
It depends on portal configuration about file storage on their sia daemon.
We can't guarantee that a file will be gone after a week.

### Can a file be downloaded more than once?

Yes, once a file is submitted to a SkyNet portal, you can download as many as you want the file.

## Disclaimer

YkShare is an experiment and under active development. The answers here may change as we get feedback from you and the project matures.