#!/usr/bin/env python

from __future__ import print_function

import os
from hashlib import sha256
import mimetypes
import base64
import json
import re
import hmac
import struct
import sys
import io

from siaskynet import Skynet

from clint.textui.progress import Bar as ProgressBar
# AES.MODE_GCM requires PyCryptodomex
from Cryptodome.Cipher import AES
from Cryptodome.Protocol.KDF import PBKDF2

### General utilities
def url_b64encode(s):
    return base64.urlsafe_b64encode(s).decode().rstrip('=')

def url_b64decode(s):
    # accept unicode (py2), str (py2) and str (py3) inputs
    s = str(s)
    s += '==='[(len(s) + 3) % 4:]
    return base64.urlsafe_b64decode(s)

def sizeof_fmt(num, suffix='B'):
    for unit in ['','Ki','Mi','Gi','Ti','Pi','Ei','Zi']:
        if abs(num) < 1024.0:
            return "%3.1f%s%s" % (num, unit, suffix)
        num /= 1024.0
    return "%.1f%s%s" % (num, 'Yi', suffix)

### Cryptography
def hkdf(length, ikm, hashfunc=sha256, salt=b"", info=b""):
    prk = hmac.new(salt, ikm, hashfunc).digest()
    t = b""
    i = 0
    okm = bytearray()
    while len(okm) < length:
        i += 1
        t = hmac.new(prk, t + info + bytes(bytearray([i])), hashfunc).digest()
        okm += t
    return bytes(okm[:length])

def create_meta_cipher(secret):
    meta_key = hkdf(16, secret, info=b'metadata')
    return AES.new(meta_key, AES.MODE_GCM, b'\x00' * 12, mac_len=16)

def file_cipher_generator(secret, salt):
    key = hkdf(16, secret, salt=salt, info=b'Content-Encoding: aes128gcm\0')
    nonce_base = hkdf(12, secret, salt=salt, info=b'Content-Encoding: nonce\0')
    seq = 0
    while True:
        if seq >= (1 << 32):
            raise ValueError("Tried to encrypt too many chunks!")
        tail, = struct.unpack('>I', nonce_base[-4:])
        tail ^= seq
        nonce = nonce_base[:-4] + struct.pack('>I', tail)
        yield AES.new(key, AES.MODE_GCM, nonce, mac_len=16)
        seq += 1

def encrypt_file_iter(secret, file, recordsize=65536):
    # 1 byte padding (minimum) + 16 byte tag
    padtaglen = 17
    assert recordsize > padtaglen, "record size too small"

    idlen = 0
    salt = os.urandom(16)
    header = struct.pack('>16sIB', salt, recordsize, idlen)
    yield header

    ciphergen = file_cipher_generator(secret, salt)
    chunksize = recordsize - padtaglen
    # this loop structure allows us to handle zero-byte files properly
    chunk = file.read(chunksize)
    while True:
        nextchunk = file.read(chunksize)
        # add padding
        if not nextchunk:
            # reached EOF, this is the last chunk
            chunk += b'\x02'
        else:
            chunk += b'\x01' + b'\x00' * (recordsize - len(chunk) - padtaglen)

        # encrypt and append GCM tag
        cipher = next(ciphergen)
        res = cipher.encrypt(chunk)
        res += cipher.digest()

        yield res

        if not nextchunk:
            break
        chunk = nextchunk

def decrypt_file_iter(secret, file):
    # ensure we read the whole header even if we get a short read
    header = bytearray()
    while len(header) < 21:
        chunk = file.read(21 - len(header))
        if not chunk:
            raise EOFError()
        header += chunk

    salt, recordsize, idlen = struct.unpack('>16sIB', header)
    # TODO handle nonzero idlen
    assert idlen == 0, "unexpected idlen"
    assert recordsize > 17, "record size too small"

    ciphergen = file_cipher_generator(secret, salt)
    while True:
        # try to get a full record if at all possible
        record = bytearray()
        while len(record) < recordsize:
            chunk = file.read(recordsize - len(record))
            if not chunk:
                break
            record += chunk
        if len(record) < 17:
            raise ValueError("Bad record received")
        record = bytes(record)

        cipher = next(ciphergen)
        res = cipher.decrypt(record[:-16])
        cipher.verify(record[-16:])

        # verify and remove padding
        res = res.rstrip(b'\x00')
        if res.endswith(b'\x01'):
            yield res[:-1]
        elif res.endswith(b'\x02'):
            # final block
            yield res[:-1]
            break
        else:
            raise ValueError("Bad padding")

### Mid-level API
class SkyNet(object):
    '''
    High-level Pythonic methods for the SkyNet API

    This class wraps the low-level API with appropriate cryptographic logic.
    '''
    def __init__(self, portal=None):
        if portal is not None:
            portal_json = json.loads(portal)
            self.portal_url = portal_json.get("portal_url")
            self.portal_upload_path = portal_json.get("portal_upload_path")
            self.portal_file_fieldname = portal_json.get("portal_file_fieldname")
            self.custom_filename = portal_json.get("custom_filename")
        else:
            default_portal = Skynet.default_upload_options()
            self.portal_url = default_portal.portal_url
            self.portal_upload_path = default_portal.portal_upload_path
            self.portal_file_fieldname = default_portal.portal_file_fieldname
            self.custom_filename = default_portal.custom_filename

    def upload(self, metadata, fileobj):
        '''
        Upload a file to the SkyNet portal.

        metadata: metadata object for the file
        fileobj: file-like object (supporting .read) to upload

        Returns: (skynet URL, secret)
        '''
        secret = os.urandom(16)

        meta_cipher = create_meta_cipher(secret)
        metadata = meta_cipher.encrypt(json.dumps(metadata).encode('utf8'))
        metadata += meta_cipher.digest()

        data = encrypt_file_iter(secret, fileobj)

        request = Skynet.upload_file_request_with_chunks(data, type('obj', (object,), {'portal_url': self.portal_url,
                                                                           'portal_upload_path': self.portal_upload_path,
                                                                           'portal_file_fieldname': self.portal_file_fieldname,
                                                                           'custom_filename': url_b64encode(metadata)
                                                                           }))
        skylink = Skynet.uri_skynet_prefix() + request.json()["skylink"]
        request.close
        return skylink, secret

    def download(self, fid, secret, fileobj, password=None, url=None):
        '''
        Download a file from the SkyNet portal.

        fid: file ID
        secret: end-to-end encryption secret
        fileobj: file-like object (supporting .write) to write to
        url: file share URL
        '''
        resp = Skynet.download_file_request(
            fid, type('obj', (object,), {'portal_url': self.portal_url}), True)
        for chunk in decrypt_file_iter(secret, resp.raw):
            fileobj.write(chunk)

    def get_metadata(self, fid, secret, url=None):
        '''
        Get file metadata.

        fid: file ID
        secret: end-to-end encryption secret
        url: file share URL
        '''
        meta_cipher = create_meta_cipher(secret)

        resp = Skynet.download_file_request(fid, type('obj', (object,), {'portal_url': self.portal_url}), True)
        metadata = json.loads(resp.headers["Skynet-File-Metadata"])
        resp.close()

        md = url_b64decode(metadata['filename'])
        md, mdtag = md[:-16], md[-16:]
        md = meta_cipher.decrypt(md)
        meta_cipher.verify(mdtag)
        metadata['metadata'] = json.loads(md)

        return metadata

def single_file_metadata(filename, filesize, mimetype='application/octet-stream'):
    return {"name": filename, "size": filesize, "type": mimetype}

### High-level CLI
def parse_url(url):
    secret = None
    m = re.match(r'^sia://([\w_-]+)/?#?([\w_-]+)?$', url)
    if m:
        portal_url = None
        fid = m.group(1)
        if m.group(2):
            secret = url_b64decode(m.group(2))
    if m is None:
        # Test if we use a portal url
        m = re.match(r'^(http|https)://(.*)/([\w_-]+)/?#?([\w_-]+)?$', url)
        if m:
            portal_url = m.group(1) + "://" + m.group(2)
            fid = m.group(3)
            if m.group(4):
                secret = url_b64decode(m.group(4))
        if m is None:
            raise Exception("Failed to parse URL %s" % url)

    return portal_url, fid, secret

def _upload(filename, file, portal=None):

    if (portal is not None):
        portal_obj = json.loads(portal)
        if (portal_obj.get("portal_url") is None or portal_obj.get("portal_url") == ''
            or portal_obj.get("portal_upload_path") is None or portal_obj.get("portal_upload_path") == ''
                or portal_obj.get("portal_file_fieldname") is None or portal_obj.get("portal_file_fieldname") == ''):
                portal = None

    skynet = SkyNet(portal)

    filename = os.path.basename(filename)

    mimetype = mimetypes.guess_type(filename, strict=False)[0] or 'application/octet-stream'
    
    # filesize
    file.seek(0, 2)
    filesize = file.tell()
    file.seek(0)

    metadata = single_file_metadata(filename, filesize, mimetype=mimetype)

    print('Uploading to:', skynet.portal_url)

    bar = ProgressBar(expected_size=filesize or 1, filled_char='=')
    class FakeFile:
        def read(self, sz=None):
            res = file.read(sz)
            bar.show(file.tell())
            return res

    url, secret = skynet.upload(metadata, FakeFile())
    print()
    url = url + '#' + url_b64encode(secret)

    print("Your download link is", url)
    return url

def get_metadata(fid, secret, url=None, service=None):
    return SkyNet(service).get_metadata(fid, secret, url)

def upload(filename, file=None, portal=None):
    '''
    Upload a file

    filename: filename to upload
    file: readable file-like object (supporting .read, .seek, .tell)
        if not specified, defaults to opening `filename`
    portal: data of the portal

    returns the share URL and owner token for the file
    '''
    if file is None:
        with open(filename, "rb") as file:
            return _upload(filename, file, portal)
    else:
        return _upload(filename, file, portal)

def download(fid, secret, dest, url=None, portal=None):
    if (portal is not None):
        portal_obj = json.loads(portal)
        if (portal_obj.get("portal_url") is None or portal_obj.get("portal_url") == ''):
            portal = None

    skynet = SkyNet(portal)

    print("Downloading from:", skynet.portal_url)
    metadata = skynet.get_metadata(fid, secret, url)

    filename = metadata['metadata']['name'];

    if os.path.isdir(dest):
        filename = os.path.join(dest, filename)
    else:
        filename = dest

    print("Downloading to %s..." % filename)

    try:
        with open(filename + ".tmp", 'wb') as outf:
            
            bar = ProgressBar(
                expected_size=metadata['metadata']['size'] or 1, filled_char='=')
            class FakeFile:
                def write(self, data):
                    res = outf.write(data)
                    bar.show(outf.tell())
                    return res
            skynet.download(fid, secret, FakeFile(), url)
    except Exception as e:
        print()
        print("File download failed:", e)
        os.unlink(filename + '.tmp')
    else:
        os.rename(filename + '.tmp', filename)
        print()
        print("Done, file verified!")

def parse_args(argv):
    import argparse

    parser = argparse.ArgumentParser(description="Download or upload a file to SkyNet")

    group = parser.add_argument_group('Common options')
    group.add_argument('target', help="URL to download or file to upload")
    group.add_argument('-s', '--skynetportal', help="SkyNet Portal to use, default is " +
                       Skynet.default_upload_options().portal_url)
    group.add_argument(
        '-o', '--output', help="Output directory or file; only relevant for download")

    group = parser.add_argument_group('General actions')
    group.add_argument('-i', '--info', action='store_true',
                       help="Get information on file. Target can be a URL or a plain file ID.")

    return parser.parse_args(argv), parser

def main(argv=None):
    if argv is None:
        argv = sys.argv[1:]
    args, parser = parse_args(argv)

    if os.path.exists(args.target):
        if args.info or args.output:
            parser.error("-i/-o must not be specified with an upload")
        print("Uploading %s..." % args.target)
        url = upload(args.target, None, args.skynetportal)
        portal, fid, secret = parse_url(url)
        return

    portal_url, fid, secret = parse_url(args.target)

    if args.info:
        metadata = get_metadata(fid, secret, args.target, portal_url)
        print("Portal: %s" % portal_url)
        print("File ID: %s" % fid)
        print("  Filename:", metadata['metadata']['name'])
        print("  MIME type:", metadata['metadata']['type'])
        print("  Size:", sizeof_fmt(metadata['metadata']['size']))
        return

    portal = None

    if (args.skynetportal is not None):
        portal = args.skynetportal
    elif (portal_url is not None):
        portal_dic = {}
        portal_dic["portal_url"] = portal_url
        portal = json.dumps(portal_dic)

    if secret:
        print("Downloading %s..." % args.target)
        download(fid, secret, args.output or '.', args.target, portal)
    else:
        # Assume they tried to upload a nonexistent file
        raise OSError("File %s does not exist" % args.target)


if __name__ == '__main__':
    exit(main())
